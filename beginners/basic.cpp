#include <iostream>

int main() {
    int a, b, c;

    std::cin >> a;
    std::cin >> b;

    c = a + b;

    std::cout << "X = " << c << std::endl;
}