#include <iostream>
#include <iomanip>
#include <cmath>

int main() {
    double r, pi = 3.14159, volume;

    std::cin >> r;
    
    volume = (4.0/3) * pi * pow(r, 3);

    std::cout << std::fixed << std::setprecision(3);
    std::cout << "VOLUME = " << volume << std::endl;
    return 0;
}