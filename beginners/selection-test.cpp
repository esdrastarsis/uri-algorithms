#include <iostream>

int main()
{
  int A, B, C, D;
  
  std::cin >> A;
  std::cin >> B;
  std::cin >> C;
  std::cin >> D;
  
  if (B > C && D > A && C + D > A + B && C > 0 && D > 0 && A % 2 == 0){
      std::cout << "Valores aceitos" << std::endl;
  } else {
      std::cout << "Valores nao aceitos" << std::endl;
  }
  return 0;
}