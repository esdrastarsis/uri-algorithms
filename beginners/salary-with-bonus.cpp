#include <iostream>
#include <iomanip>

int main() {
    std::string name;
    double salary, sales, total;

    std::cin >> name;
    std::cin >> salary;
    std::cin >> sales;

    total = salary + (sales * 0.15);

    std::cout << std::fixed << std::setprecision(2);
    std::cout << "TOTAL = R$ " << total << std::endl;
    return 0;
}