#include <iostream>
#include <iomanip>

int main() {
    double a, b, p1=3.5, p2=7.5, m;

    std::cin >> a;
    std::cin >> b;

    m = ((a * p1) + (b * p2)) / (p1 + p2);

    std::cout << std::fixed << std::setprecision(5);
    std::cout << "MEDIA = " << m << std::endl;
    return 0;
}