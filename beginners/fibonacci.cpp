#include <iostream>

int main() {

    unsigned int seq;
    unsigned int a = 0, b = 1;
    unsigned int lim;
    int n;

    std::cin >> lim;

    if (lim > 0 && lim < 46){
        std::cout << a << " ";
        std::cout << b << " ";
        for(n = 2; n < lim; n++){
            seq = (a + b);
            if(n >= lim - 1){
                std::cout << seq << std::endl;
            } else {
                std::cout << seq << " ";
            }
            a = b;
            b = seq;
        }
    }
    return 0;
}