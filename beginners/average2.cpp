#include <iostream>
#include <iomanip>

int main() {
    double a, b, c, p1=2, p2=3, p3=5, m;

    std::cin >> a;
    std::cin >> b;
    std::cin >> c;

    m = ((a * p1) + (b * p2) + (c * p3)) / (p1 + p2 + p3);

    std::cout << std::fixed << std::setprecision(1);
    std::cout << "MEDIA = " << m << std::endl;
    return 0;
}