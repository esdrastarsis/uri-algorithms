#include <iostream>

int main(){
    int ddd_code;
    
    std::cin >> ddd_code;
    
    if(ddd_code == 61){
        std::cout << "Brasilia" << std::endl;
    }else if(ddd_code == 71){
        std::cout << "Salvador" << std::endl;
    }else if(ddd_code == 11){
        std::cout << "Sao Paulo" << std::endl;
    }else if(ddd_code == 21){
        std::cout << "Rio de Janeiro" << std::endl;
    }else if(ddd_code == 32){
        std::cout << "Juiz de Fora" << std::endl;
    }else if(ddd_code == 19){
        std::cout << "Campinas" << std::endl;
    }else if(ddd_code == 27){
        std::cout << "Vitoria" << std::endl;
    }else if(ddd_code == 31){
        std::cout << "Belo Horizonte" << std::endl;
    } else {
        std::cout << "DDD nao cadastrado" << std::endl;
    }
    return 0;
}