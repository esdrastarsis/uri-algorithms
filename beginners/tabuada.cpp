#include <iostream>

int main() {
    int num;

    std::cin >> num;

    for(int i = 1; i < 11; i++){
        std::cout << i << " x " << num << " = " << i * num << std::endl;
    }
    return 0;
}