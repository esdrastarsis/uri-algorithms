#include <iostream>
#include <cmath>
#include <iomanip>

int main() {
    double a, b, c;
    std::cin >> a;
    std::cin >> b;
    std::cin >> c;

    double delta = pow(b, 2) - 4 * a * c;

    if(delta <= 0.0 || a == 0){
        std::cout << "Impossivel calcular" << std::endl;
    } else {
        double raiz1 = (-b + sqrt(delta)) / (2 * a);
        double raiz2 = (-b - sqrt(delta)) / (2 * a);
        std::cout << std::fixed << std::setprecision(5);
        std::cout << "R1 = " << raiz1 << std::endl;
        std::cout << "R2 = " << raiz2 << std::endl;
    }

    return 0;
}