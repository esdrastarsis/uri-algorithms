#include <iostream>
#include <iomanip>

int main() {
    int n, h;
    double v, s;

    std::cin >> n;
    std::cin >> h;
    std::cin >> v;

    s = h * v;

    std::cout << "NUMBER = " << n << std::endl;
    std::cout << std::fixed << std::setprecision(2);
    std::cout << "SALARY = U$ " << s << std::endl;
    return 0;
}