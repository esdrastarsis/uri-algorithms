#include <iostream>
#include <cmath>

int main() {
    double a, b, c;
    int checked = 0;

    std::cin >> a;
    std::cin >> b;
    std::cin >> c;

    for(int i = 0; i < 2; i++){
        if(a >= b+c){
            if (checked == 0){
                std::cout << "NAO FORMA TRIANGULO" << std::endl;
            }
            checked = 1;
        } else if(pow(a, 2) == pow(b, 2) + pow(c, 2)){
            if (checked == 1){
                std::cout << "TRIANGULO RETANGULO" << std::endl;
                checked = 2;
            }
        } else if(pow(a, 2) >= pow(b, 2) + pow(c, 2)){
            if (checked == 2){
                std::cout << "TRIANGULO OBTUSANGULO" << std::endl;
                checked = 3;
            }
        } else if(pow(a, 2) <= pow(b, 2) + pow(c, 2)){
            if (checked == 3){
                std::cout << "TRIANGULO ACUTANGULO" << std::endl;
                checked = 4;
            }
        } else if(a == b && b == c) {
            if (checked == 4){
                std::cout << "TRIANGULO EQUILATERO" << std::endl;
                checked = 5;
            }
        } else if(a == b || b == c || a == c) {
            if (checked == 5){
                std::cout << "TRIANGULO ISOSCELES" << std::endl;
                checked = 6;
            }
        }
    }
    std::cout << "Checked = " << checked << std::endl;
    return 0;
}