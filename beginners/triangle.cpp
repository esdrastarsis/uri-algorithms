#include <iostream>
#include <iomanip>
#include <cmath>

int main() {
    double a, b, c, area, perimeter;

    std::cin >> a;
    std::cin >> b;
    std::cin >> c;

    perimeter = a + b + c;
    area = ((a+b) * c) / 2;

    std::cout << std::fixed << std::setprecision(1);

    if(a < b + c && a > fabs(b - c)){
        if(b < a + c && b > fabs(a - c)){
            if(c < a + b && c > fabs(a - b))
                std::cout << "Perimetro = " << perimeter << std::endl;
        }
    } else
        std::cout << "Area = " << area << std::endl;
    return 0;
}