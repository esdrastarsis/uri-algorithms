#include <iostream>

int main() {
    int x,y;

    while(std::cin) {
        std::cin >> x;
        std::cin >> y;
        if(x > y)
            std::cout << "Decrescente" << std::endl;
        else if(x < y)
            std::cout << "Crescente" << std::endl;
        else
            break;
    }
    return 0;
}