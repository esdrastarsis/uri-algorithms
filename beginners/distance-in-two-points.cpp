#include <iostream>
#include <cmath>
#include <iomanip>

int main() {
	double x1, x2, y1, y2, distance;

	std::cin >> x1;
	std::cin >> y1;
	std::cin >> x2;
	std::cin >> y2;

	distance = sqrt(((pow(x2, 2)) - (pow(x1, 2))) + ((pow(y2, 2)) - (pow(y1, 2))));

	std::cout << std::fixed << std::setprecision(4);
	std::cout << "x1 = " << x1 << std::endl;
	std::cout << "y1 = " << y1 << std::endl;
	std::cout << "x2 = " << x2 << std::endl;
	std::cout << "y2 = " << y2 << std::endl;
	std::cout << "distance = " << distance << std::endl;	
 	return 0;
}
