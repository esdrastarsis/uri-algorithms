#!/bin/bash
if [ $# -gt 1 ]; then
    echo "Invalid argument"
else
    clang++ -std=c++17 -stdlib=libc++ $* -o binary
    ./binary
    rm binary
fi