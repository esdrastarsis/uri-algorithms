#include <iostream>
#include <iomanip>
#include <cmath>

int main() {
    double a, b, c, t, ci, tr, q, r, pi = 3.14159;

    std::cin >> a;
    std::cin >> b;
    std::cin >> c;

    t = (a * c) / 2;
    ci = pi * pow(c, 2);
    tr = ((a + b) * c) / 2;
    q = pow(b, 2);
    r = a * b;

    std::cout << std::fixed << std::setprecision(3);
    std::cout << "TRIANGULO: " << t << std::endl;
    std::cout << "CIRCULO: " << ci << std::endl;
    std::cout << "TRAPEZIO: " << tr << std::endl;
    std::cout << "QUADRADO: " << q << std::endl;
    std::cout << "RETANGULO: " << r << std::endl;

    return 0;
}