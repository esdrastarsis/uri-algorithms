#include <iostream>
#include <cmath>

int main(){
    int num;

    std::cin >> num;

    for(int i = 1; i < num + 1; i++){
        std::cout << i << " " << pow(i, 2) << " " << pow(i, 3) << std::endl;
    }
    return 0;
}