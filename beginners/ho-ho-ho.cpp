#include <iostream>
#include <cmath>

int main() {

    unsigned int ent, lim;
    int n;

    std::cin >> ent;
    lim = pow(10, 6);

    if (ent > 0 && ent < lim){
        for(n = 0; n < ent; n++){
            if(n >= ent - 1){
                std::cout << "Ho!" << std::endl;
            } else {
                std::cout << "Ho" << " ";
            }
        }
    }
    return 0;
}