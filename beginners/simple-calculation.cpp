#include <iostream>
#include <iomanip>

int main() {
	int p1_code, p1_num, p2_code, p2_num;
	double p1_value, p2_value, total_value;

	std::cin >> p1_code;
	std::cin >> p1_num;
	std::cin >> p1_value;
	std::cin >> p2_code;
	std::cin >> p2_num;
	std::cin >> p2_value;
	
	total_value = (p1_value * p1_num) + (p2_value * p2_num);
		
	std::cout << std::fixed << std::setprecision(2);
	std::cout << "VALOR A PAGAR: R$ " << total_value << std::endl;
	return 0;
}
