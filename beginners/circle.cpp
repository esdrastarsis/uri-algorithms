#include <iostream>
#include <cmath>
#include <iomanip>

int main() {
    double pi = 3.14159;
    double radius;
    double area;

    std::cin >> radius;

    area = pi * pow(radius, 2);

    std::cout << std::fixed << std::setprecision(4);
    std::cout << "A=" << area << std::endl;
    return 0;
}